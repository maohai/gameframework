using GameClient;
using GameClient.NetWork;
using UnityEngine;

namespace UI
{
    public class MainBoard : SingleUIDialog<MainBoard>
    {
        public static void Show()
        {
            dialog.Show("MainBoard",false);
        }

        protected override void OnUIShow(params object[] param)
        {
            base.OnUIShow(param);
            Debug.Log("MainBoard");
            
            UITips.Show("huanyingguangling");
        }

        protected override void OnUIResume()
        {
            base.OnUIResume();
            Debug.Log("onUIResume");
        }

        public void OnBtnClick()
        {
            TestDialog.Show();
        }
    }

}