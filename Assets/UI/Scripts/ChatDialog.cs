using GameClient;
using GameClient.NetWork;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
	public class ChatDialog : UIDialog
	{
		public static void Show()
		{
			dialog.Show("ChatDialog",true);
		}

		[SerializeField]
		private InputField inputMessage;

		[SerializeField]
		private Transform contentMsg;

		[SerializeField]
		private GameObject goMsgItem;

		protected override void OnUIShow(params object[] param)
		{
			base.OnUIShow(param);
			NetWorkModule.EventServerMessage.Register(OnMessage);
		}

		protected override void OnUIClose()
		{
			base.OnUIClose();
			NetWorkModule.EventServerMessage.Remove(OnMessage);;
		}

		public void OnSend()
		{
			if (inputMessage.text == "")
			{
				return;
			}

			string id = Client.Ins.NetWork.ID;
			this.Req(PropType.CHAT,GameProp.TRANSFORM_CREQ,new MessageDto(){Msg = inputMessage.text,Id = id});
			var msg = Instantiate(goMsgItem, contentMsg);
			msg.transform.GetChild(0).GetComponent<Text>().text =$"<color=red> {id}</color>:{inputMessage.text}";
			inputMessage.text = "";
			msg.SetActive(true);
		}
		
		
		private void OnMessage(object[] data)
		{
			SocketModel model = (SocketModel)data[0];
			if (model.type == PropType.CHAT && model.command == ChatProp.MESSAGE_SERS)
			{
				var msgDto = model.GetMessage<MessageDto>();
				string id = Client.Ins.NetWork.ID;
				var msg = Instantiate(goMsgItem, contentMsg);
				string color = id == msgDto.Id ? "red" : "yellow";
				msg.transform.GetChild(0).GetComponent<Text>().text = $"<color={color}> {msgDto.Id}</color>:{msgDto.Msg}";
				msg.SetActive(true);
			}
		}
	}

}