using GameClient;
using UnityEngine;

namespace UI
{
    public class TestDialog : UIAniDialog
    {
        public static void Show()
        {
            dialog.Show("TestDialog",true);
        }

        protected override void OnUIShow(params object[] param)
        {
            base.OnUIShow(param);
            Debug.Log("onuishow");
            
            UITips.Show("onuishow");
        }
        
        protected override void OnUIShowCompile()
        {
            base.OnUIShowCompile();
            Debug.Log("OnUIShowCompile");
        }
        
        protected override void OnUIResume()
        {
            base.OnUIResume();
            Debug.Log("onUIResume");
        }
        
        
        protected override void OnUIClose()
        {
            base.OnUIClose();
            Debug.Log("onuiClose");
            
            UITips.Show("onuiClose");
        }

        public void OnBtnCloseClick()
        {
            Close();
        }
    }

}