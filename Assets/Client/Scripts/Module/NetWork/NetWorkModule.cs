using System;
using System.Collections.Generic;
using System.Net.Sockets;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace GameClient.NetWork
{
    public class NetWorkModule : MonoBehaviour,IMod
    {
        public static readonly ClientEvent EventServerMessage=new ClientEvent();
        
        public string ID => id;
        private byte[] readBuff = new byte[1024];
        private Socket socket;
        private List<SocketModel> messages = new List<SocketModel>();
        private bool isReading = false;
        private string id;
        private List<byte> cache = new List<byte>();

        private static long msgID;
        private Dictionary<long, Action<SocketModel>> writeCb = new Dictionary<long, Action<SocketModel>>();

        public void Init(JObject db, JObject saveData, Action cb)
        {
            StartServer();
            cb?.Invoke();
        }

        public JObject GetSaveData()
        {
            return null;
        }
        
        private void StartServer()
        {
            if (!Client.Ins.Config.StarSocketServer)
            {
                return;
            }
            try
            {
                socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                var ipInfo = Client.Ins.Config.ServerIpAddress.Split(":");
                string serverIp = ipInfo[0];
                int port =int.Parse(ipInfo[1]);
                Debug.Log("连接" + serverIp);
                socket.Connect(serverIp, port);
                socket.BeginReceive(readBuff, 0, 1024, SocketFlags.None, ReceiveCallBack, readBuff);
            }
            catch (Exception e)
            {
                Debug.LogError("连接失败"+e.Message);
            }
        }
        private void Update()
        {
            while (messages.Count>0)
            {
                SocketModel model = messages[0];
                StartCoroutine("MessageReceive", model);
                messages.RemoveAt(0);
            }
        }
        private void MessageReceive(SocketModel model)
        {
            Debug.Log("Server:{"+model+"}");
            var cb = GetWriteCb(model);
            if (cb!=null)
            {
                if (model.type == PropType.Message)//服务器发送的消息
                {
                   UITips.Show(model.GetMessage<MsgDto>().msg);
                }
                else
                {
                    cb.Invoke(model);
                }
            }
            else
            {
                if (model.type == PropType.LOGIN && model.command == LoginProp.LOGIN_SRES)
                {
                    Debug.Log(model.GetMessage<MsgDto>().msg);
                    id = model.GetMessage<MsgDto>().msg;
                    return;
                }
                EventServerMessage.Trigger(model);
            }
        }
        
        private void ReceiveCallBack(IAsyncResult ar)
        {
            int length = socket.EndReceive(ar);
            byte[] message = new byte[length];
            
            Buffer.BlockCopy(readBuff, 0, message, 0, length);
            cache.AddRange(message);
            if (!isReading)
            {
                isReading = true;
                OnData();
                socket.BeginReceive(readBuff, 0, 1024, SocketFlags.None, ReceiveCallBack, readBuff);
            }
        }

        private void OnData()
        {
            byte[] buff = null;
            buff = LengthEncoding.Decode(ref cache);
            if (buff == null)
            {
                isReading = false;
                return;
            }
            SocketModel message = MessageEncoding.Decode(buff);
            if (message == null)
            {
                isReading = false;
                return;
            }
            messages.Add(message);
            OnData();
        }

        private void Close()
        {
            if (socket != null)
            {
                Debug.Log("关闭socket");
                socket.Dispose();
                socket.Close();
            }
        }

        private void OnDestroy()
        {
            Close();
        }
        private void Req(SocketModel model,Action<SocketModel> cb=null)
        {
            if (socket==null||!socket.Connected)
            {
                UITips.Show("网络未连接");
                cb = null;
                StartServer();
                return;
            }
            Debug.Log("Client:{"+model+"}");
            ByteArray arr = new ByteArray();
            byte[] ba = MessageEncoding.Encode(model);
            arr.Write(ba.Length);
            arr.Write(ba);
            try
            {
                socket.Send(arr.GetBuffer());
                PushWriteCb(model, cb);
            }
            catch (Exception e)
            {
                Debug.LogError("网络错误"+e.Message);
                cb?.Invoke(null);
            }
            arr.Close();
        }

        public void Req(PropType type, int command, object msg,Action<SocketModel> cb=null)
        {
            msgID++;
            Req(new SocketModel(msgID,type, -1, command, msg),cb);
        }
        public void Req(PropType type, int area, int command, object msg,Action<SocketModel> cb=null)
        {
            msgID++;
            Req(new SocketModel(msgID,type, area, command, msg),cb);
        }


        private void PushWriteCb(SocketModel clientModel,Action<SocketModel> cb)
        {
            if (clientModel.id <0 ||cb==null)//没有回调
            {
                return;
            }
            long k = clientModel.id;
            if (!writeCb.ContainsKey(k))
            {
                writeCb.Add(k,cb);
            }
        }

        private Action<SocketModel> GetWriteCb(SocketModel serverModel)
        {
            long k = serverModel.id;
            if (writeCb.ContainsKey(k))
            {
                var value = writeCb[k];
                writeCb.Remove(k);
                return value;
            }
            return null;
        }
    }
}