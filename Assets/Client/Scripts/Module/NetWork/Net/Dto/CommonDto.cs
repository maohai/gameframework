using System;

namespace GameClient.NetWork
{
    /// <summary>
    /// 消息协议
    /// </summary>
    [Serializable]
    public class MsgDto
    {
        public bool success;
        public string msg;

        public MsgDto(bool success, string msg)
        {
            this.success = success;
            this.msg = msg;
        }
    }

    /// <summary>
    /// 错误消息
    /// </summary>
    [Serializable]
    public class ErrorDto
    {
        public string errorMsg;
    }
}