using System;

namespace GameClient.NetWork
{
    /// <summary>
    /// 账号信息
    /// </summary>
    [Serializable]
    public class LoginInfoDto
    {
        public string account;
        public string password;
    }

    /// <summary>
    /// 注册信息
    /// </summary>
    [Serializable]
    public class RegisterInfoDto
    {
        public string account;
        public string password;
    }

    [Serializable]
    public class TransformDto
    {
        public float[] position;
        public float[] rotation;
    }
}