using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace GameClient
{
    public class DialogModule : MonoBehaviour, IMod
    {
        [SerializeField] private RectTransform trUIContent;
        private List<UIDialog> _dialogs;

        private UIDialog _topDialog;
        public UIDialog TopDialog => _topDialog;

        private AssetBundle asset;

        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }

        public void Init(JObject db, JObject saveData, Action cb)
        {
            _dialogs = new List<UIDialog>();
#if !UNITY_EDITOR
            StartCoroutine(LoadAssetBundle(cb));
#else
            cb?.Invoke();
#endif
        }
        private IEnumerator LoadAssetBundle(Action cb)
        {
            var result = AssetBundle.LoadFromFileAsync(Path.Combine(Application.persistentDataPath, "Update",
                "AssetBundles", "uidialog"));
            yield return result;
            asset = result.assetBundle;
            cb?.Invoke();
        }

        public JObject GetSaveData()
        {
            return null;
        }

        /// <summary>
        /// 展示界面
        /// </summary>
        /// <param name="uiPath"></param>
        /// <param name="param"></param>
        public void Show(string dialogName, bool isBlack = true, params object[] param)
        {
#if UNITY_EDITOR
            var obj = Resources.Load<GameObject>(dialogName);
#else
            var obj = asset.LoadAsset<GameObject>(dialogName);
#endif
            var tr = GetDialogParent(isBlack, dialogName);
            CreateDialogClickMask(tr);
            GameObject goDialog = Instantiate(obj, tr);
            goDialog.transform.SetSiblingIndex(0);
            UIDialog dialog = goDialog.GetComponent<UIDialog>();
            _dialogs.Add(dialog);
            _topDialog = dialog;
            if (TryFun(dialog, "OnUIShow", out var fun))
            {
                fun.Invoke(dialog, new object[] {param});
            }
        }

        /// <summary>
        /// 从顶部开始关闭界面，会保留最后一个
        /// </summary>
        /// <returns></returns>
        public bool Back()
        {
            if (_dialogs.Count == 1)
            {
                return false;
            }

            DestroyLast();
            ResumeLast();
            return true;
        }

        /// <summary>
        /// 返回到某个界面
        /// </summary>
        /// <param name="uiDialog"></param>
        public void BackTo(UIDialog uiDialog)
        {
            int count = _dialogs.Count;
            if (_dialogs.Count == 0)
            {
                return;
            }

            int index = _dialogs.IndexOf(uiDialog);

            if (index == -1)
            {
                return;
            }

            //删除所以上层dialog
            for (int i = 0; i < count - index - 1; i++)
            {
                DestroyLast();
            }

            ResumeLast();
        }

        /// <summary>
        /// 关闭界面
        /// </summary>
        /// <param name="dialog"></param>
        public void Close(UIDialog dialog)
        {
            if (_dialogs.Count == 0)
            {
                return;
            }

            int index = _dialogs.IndexOf(dialog);
            if (index == -1)
            {
                return;
            }

            _dialogs.RemoveAt(index);
            if (TryFun(dialog, "OnUIClose", out var fun))
            {
                fun.Invoke(dialog, null);
            }

            Destroy(dialog.tr.parent.gameObject);

            if (index - 1 >= 0)
            {
                var d = _dialogs[index - 1];
                if (TryFun(d, "OnUIResume", out var f))
                {
                    f.Invoke(d, null);
                }
            }
        }

        private void DestroyLast()
        {
            UIDialog dialog = _dialogs.Last();
            _dialogs.Remove(dialog);
            if (TryFun(dialog, "OnUIClose", out var fun))
            {
                fun.Invoke(dialog, null);
            }

            Destroy(dialog.tr.parent.gameObject);
        }

        private void ResumeLast()
        {
            var d = _dialogs.Last();
            if (d != null)
            {
                if (TryFun(d, "OnUIResume", out var fun))
                {
                    fun.Invoke(d, null);
                }
            }
        }

        private bool TryFun(UIDialog dialog, string funName, out MethodInfo fun)
        {
            ;
            BindingFlags flag = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.InvokeMethod;
            fun = dialog.GetType().GetMethod(funName, flag);
            if (fun != null)
            {
                return true;
            }

            return false;
        }

        private RectTransform GetDialogParent(bool isBlack, string name)
        {
            var go = new GameObject("Dialog_" + name, typeof(RectTransform));
            var tr = (RectTransform) go.transform;
            tr.anchorMax = Vector2.one;
            tr.anchorMin = Vector2.zero;
            tr.pivot = Vector2.one * 0.5f;
            tr.offsetMax = Vector2.zero;
            tr.offsetMin = Vector2.zero;
            tr.SetParent(trUIContent, false);
            var canvas = go.AddComponent<Canvas>();
            canvas.overrideSorting = true;
            canvas.sortingOrder = _dialogs.Count * 20;
            go.AddComponent<GraphicRaycaster>();
            if (isBlack)
            {
                var image = tr.gameObject.AddComponent<Image>();
                image.color = new Color(0, 0, 0, 0.6f);
            }

            return tr;
        }

        private void CreateDialogClickMask(Transform partner)
        {
            var go = new GameObject("Mask", typeof(RectTransform));
            var tr = (RectTransform) go.transform;
            tr.anchorMax = Vector2.one;
            tr.anchorMin = Vector2.zero;
            tr.pivot = Vector2.one * 0.5f;
            tr.offsetMax = Vector2.zero;
            tr.offsetMin = Vector2.zero;
            tr.SetParent(partner, false);
            var canvas = go.AddComponent<Canvas>();
            canvas.overrideSorting = true;
            canvas.sortingOrder = _dialogs.Count * 20 + 19;
            go.AddComponent<GraphicRaycaster>();
            var image = tr.gameObject.AddComponent<Image>();
            image.color = new Color(0, 0, 0, 0);
        }
    }
}