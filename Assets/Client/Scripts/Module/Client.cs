using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using Newtonsoft.Json.Linq;
using UnityEngine;
using System.Security.Cryptography;
using Base;
using GameClient.NetWork;

namespace GameClient
{
    public class Client : SingleBehaviour<Client>
    {
        public static void Run()
        {
            Debug.Log("加载UI");
#if UNITY_EDITOR
            string filePath = Path.Combine(Application.persistentDataPath, "Update","AssetBundles", "init");
            AssetBundle ab = AssetBundle.LoadFromFile(filePath);      
#else
            //加载所需程序集
            string clientGame=Path.Combine(Application.persistentDataPath,"Update", "HotUpdateDlls", "ClientGame.dll.bytes");
            Assembly.Load(File.ReadAllBytes(clientGame));
            string clientUI=Path.Combine(Application.persistentDataPath,"Update", "HotUpdateDlls", "ClientUI.dll.bytes");
            Assembly.Load(File.ReadAllBytes(clientUI));

            //加载init assetBundle
            string filePath = Path.Combine(Application.persistentDataPath, "Update","AssetBundles", "init");
            AssetBundle ab = AssetBundle.LoadFromFile(filePath);
#endif
            var uiContent = ab.LoadAsset<GameObject>("UICanvas");
            Instantiate(uiContent);
        }
        
        /// <summary>
        /// 客户端加载完成事件
        /// </summary>
        public event Action EventLoadCompile;

        private ClientConfig _config;

        public ClientConfig Config => _config;

        public readonly PlayerModule Player=new PlayerModule();
        public readonly LocalizationModule Localization=new LocalizationModule();
        
        public DialogModule Dialog;
        [HideInInspector]
        public NetWorkModule NetWork;

        public EventModule Event=new EventModule();

        
        private List<IMod> _mods = new List<IMod>();

        private bool isLoadComplete;

        protected override void Awake()
        {
            base.Awake();
            _config = Resources.Load<ClientConfig>("ClientConfig");
            _mods.Add(Event);
            _mods.Add(Localization);
            _mods.Add(Dialog);
            NetWork = gameObject.AddComponent<NetWorkModule>();
            _mods.Add(NetWork);
            _mods.Add(Player);
            Init();
            DontDestroyOnLoad(gameObject);
        }

        private readonly string SAVE_KEY = "QWERABCD";

        private int curLoadIndex = 0;//当前加载第几个模块

        private void Init()
        {
#if UNITY_EDITOR
            TextAsset t = Resources.Load<TextAsset>("Data");
#else
            //加载data assetBundle
            string filePath = Path.Combine(Application.persistentDataPath, "Update","AssetBundles", "data");
            var dataAB = AssetBundle.LoadFromMemory(File.ReadAllBytes(filePath));
            TextAsset t = dataAB.LoadAsset<TextAsset>("Data");
#endif
            JObject jo = JObject.Parse(t.text);
            jo = CustomParse(jo);
#if UNITY_EDITOR
            Resources.UnloadAsset(t);
#else
            dataAB.Unload(true);
#endif
            JObject saveData = new JObject();
            var savePath = GetSavePath();
            if (File.Exists(savePath))
            {
                FileStream fs = new FileStream(savePath, FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                long start = br.BaseStream.Position;
                //获取数据长度
                br.BaseStream.Seek(0, SeekOrigin.End);
                long end = br.BaseStream.Position;
                int byteCount = (int) (end - start);
                br.BaseStream.Seek(0, SeekOrigin.Begin);
                byte[] b = br.ReadBytes(byteCount);
                br.Close();
                fs.Close();
                try
                {
                    if (Config.SaveDataEncryption)
                    {
                        b = AESDecrypt(b, SAVE_KEY);
                    }
                    string data = Encoding.UTF8.GetString(b);
                    saveData = JObject.Parse(data);
                }
                catch (Exception e)
                {
                    Debug.Log(e);
                    saveData = new JObject();
                }
            }
            InitModule(jo, saveData);
        }

        //递归初始化模块
        private void InitModule(JObject jo, JObject saveData, int i = 0)
        {
            curLoadIndex = i;
            if (i >= _mods.Count) //所有模块初始化完成
            {
                OnInitModuleCompile();
                return;
            }
            var modName = _mods[i].GetType().Name;
            long startTimestamp = Utility.GetCurTimestampMilliseconds();
            JObject d = (JObject) saveData[modName];
            if (d == null)
            {
                d = new JObject();
            }
            _mods[i].Init(jo, d, () =>
            {
                long endTimestamp = Utility.GetCurTimestampMilliseconds();
                Debug.Log($"加载模块{modName} 耗时{endTimestamp-startTimestamp}ms");
                InitModule(jo, saveData, ++i);
            });
        }

        private void OnInitModuleCompile()
        {
            Debug.Log("所有模块初始化完成");
            isLoadComplete = true;
            EventLoadCompile?.Invoke();
        }


        #region 解析、保存数据
        public void SaveData()
        {
            var data = new JObject();
            for (int i = 0; i < _mods.Count; i++)
            {
                var m = _mods[i];
                var d = m.GetSaveData();
                if (d != null)
                {
                    data[m.GetType().Name] = d;
                }
            }

            string saveDataPath = GetSavePath();
            Debug.Log("保存路径" + saveDataPath);
            FileStream stream = new FileStream(saveDataPath, FileMode.Create);
            byte[] bytes = Encoding.UTF8.GetBytes(data.ToString());
            if (Config.SaveDataEncryption)
            {
                bytes  = AESEncrypt(bytes, SAVE_KEY);
            }
            stream.Write(bytes, 0, bytes.Length);
            stream.Close();
        }
        
        private JObject CustomParse(JObject jo)
        {
            JObject newJo = new JObject();
            foreach (var keyValuePair in jo)
            {
                JArray data = keyValuePair.Value.ToObject<JArray>();

                string[] names = data[0].ToObject<string[]>(); //属性名
                JArray j = new JArray();
                for (int y = 1; y < data.Count; y++)
                {
                    JObject d = new JObject();
                    for (int x = 0; x < names.Length; x++)
                    {
                        d[names[x]] = data[y].ToObject<JArray>()[x];
                    }

                    j.Add(d);
                }

                newJo[keyValuePair.Key] = j;
            }

            return newJo;
        }

        private string GetSavePath()
        {
            return Path.Combine(Application.persistentDataPath, "SaveData");
        }

        /// <summary>
        /// AES 加密(高级加密标准，是下一代的加密算法标准，速度快，安全级别高，目前 AES 标准的一个实现是 Rijndael 算法)
        /// </summary>
        /// <param name="EncryptString">待加密密文</param>
        /// <param name="EncryptKey">加密密钥</param>
        private byte[] AESEncrypt(byte[] EncryptByte, string EncryptKey)
        {
            if (EncryptByte.Length == 0)
            {
                throw (new Exception("明文不得为空"));
            }

            if (string.IsNullOrEmpty(EncryptKey))
            {
                throw (new Exception("密钥不得为空"));
            }

            byte[] m_strEncrypt;
            byte[] m_btIV = Convert.FromBase64String("Rkb4jvUy/ye7Cd7k89QQgQ==");
            byte[] m_salt = Convert.FromBase64String("gsf4jvkyhye5/d7k8OrLgM==");
            Rijndael m_AESProvider = Rijndael.Create();
            try
            {
                MemoryStream m_stream = new MemoryStream();
                PasswordDeriveBytes pdb = new PasswordDeriveBytes(EncryptKey, m_salt);
                ICryptoTransform transform = m_AESProvider.CreateEncryptor(pdb.GetBytes(32), m_btIV);
                CryptoStream m_csstream = new CryptoStream(m_stream, transform, CryptoStreamMode.Write);
                m_csstream.Write(EncryptByte, 0, EncryptByte.Length);
                m_csstream.FlushFinalBlock();
                m_strEncrypt = m_stream.ToArray();
                m_stream.Close();
                m_stream.Dispose();
                m_csstream.Close();
                m_csstream.Dispose();
            }
            catch (IOException ex)
            {
                throw ex;
            }
            catch (CryptographicException ex)
            {
                throw ex;
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                m_AESProvider.Clear();
            }

            return m_strEncrypt;
        }

        // 解密字节数组
        /// <summary>
        /// AES 解密(高级加密标准，是下一代的加密算法标准，速度快，安全级别高，目前 AES 标准的一个实现是 Rijndael 算法)
        /// </summary>
        /// <param name="DecryptString">待解密密文</param>
        /// <param name="DecryptKey">解密密钥</param>
        private byte[] AESDecrypt(byte[] DecryptByte, string DecryptKey)
        {
            if (DecryptByte.Length == 0)
            {
                throw (new Exception("密文不得为空"));
            }

            if (string.IsNullOrEmpty(DecryptKey))
            {
                throw (new Exception("密钥不得为空"));
            }

            byte[] m_strDecrypt;
            byte[] m_btIV = Convert.FromBase64String("Rkb4jvUy/ye7Cd7k89QQgQ==");
            byte[] m_salt = Convert.FromBase64String("gsf4jvkyhye5/d7k8OrLgM==");
            Rijndael m_AESProvider = Rijndael.Create();
            try
            {
                MemoryStream m_stream = new MemoryStream();
                PasswordDeriveBytes pdb = new PasswordDeriveBytes(DecryptKey, m_salt);
                ICryptoTransform transform = m_AESProvider.CreateDecryptor(pdb.GetBytes(32), m_btIV);
                CryptoStream m_csstream = new CryptoStream(m_stream, transform, CryptoStreamMode.Write);
                m_csstream.Write(DecryptByte, 0, DecryptByte.Length);
                m_csstream.FlushFinalBlock();
                m_strDecrypt = m_stream.ToArray();
                m_stream.Close();
                m_stream.Dispose();
                m_csstream.Close();
                m_csstream.Dispose();
            }
            catch (IOException ex)
            {
                throw ex;
            }
            catch (CryptographicException ex)
            {
                throw ex;
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                m_AESProvider.Clear();
            }

            return m_strDecrypt;
        }

        #endregion
        
        private void OnApplicationQuit()
        {
            Debug.Log("退出");
            SaveData();
        }


        /// <summary>
        /// 获取加载进度（0-1）若为1则加载完成
        /// </summary>
        /// <returns></returns>
        public float GetLoadProgress()
        {
            if (_mods.Count == 0)
            {
                return 1;
            }

            if (isLoadComplete)
            {
                return 1;
            }
            return curLoadIndex * 1f / _mods.Count-0.01f;
        }

        /// <summary>
        /// 延时多少秒执行
        /// </summary>
        /// <param name="cb"></param>
        /// <param name="t"></param>
        public void Wait(Action cb, float t)
        {
            StartCoroutine(_Wait(cb, t));
        }

        private IEnumerator _Wait(Action cb, float t)
        {
            yield return new WaitForSeconds(t);
            cb?.Invoke();
        }
    }
}