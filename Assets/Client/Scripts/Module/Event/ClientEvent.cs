using System;

namespace GameClient {
    
    public class ClientEvent {
        private static int EvtID = 0;

        public readonly int ID = ++EvtID;
        
        public void Register(Action<object[]> listener) {
            if (Client.Ins != null) {
                Client.Ins.Event.Register(this, listener);
            }
        }

        public void Remove(Action<object[]> listener) {
            if (Client.Ins != null) {
                Client.Ins.Event.Remove(this, listener);
            }
        }

        public void RemoveAll() {
            if (Client.Ins != null) {
                Client.Ins.Event.RemoveAll(this);
            }
        }

        public void Trigger(params object[] args) {
            if (Client.Ins != null) {
                Client.Ins.Event.Trigger(this, args);
            }
        }
    }
}