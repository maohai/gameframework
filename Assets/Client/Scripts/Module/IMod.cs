
using System;
using Newtonsoft.Json.Linq;

public interface IMod
{
    void Init(JObject db,JObject saveData, Action cb);

    JObject GetSaveData();
}
