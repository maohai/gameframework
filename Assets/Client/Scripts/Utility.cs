using System;

namespace GameClient
{
    public class Utility
    {
        /// <summary>
        /// 获取当前时间戳(毫秒)
        /// </summary>
        /// <returns></returns>
        public static long GetCurTimestampMilliseconds()
        {
            return (long)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds;
        }
        
        /// <summary>
        /// 获取当前时间戳（秒）
        /// </summary>
        /// <returns></returns>
        public static long GetCurTimestampSeconds()
        {
            return (long)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;
        }
    }
}
