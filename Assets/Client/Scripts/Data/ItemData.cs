
using Newtonsoft.Json.Linq;

namespace GameClient.Data
{
    public class ItemData
    {
		/// <summary>
		/// 物品ID
		/// </summary>
		public int ID{ get; set; }

        public ItemData(JToken data)
        {
			ID = data["ID"].ToObject<int>();
        }
    }
}
