
using Newtonsoft.Json.Linq;

namespace GameClient.Data
{
    public class MonsterData
    {
		/// <summary>
		/// 怪物类型
		/// </summary>
		public int MonsterType{ get; set; }
		/// <summary>
		/// 名字
		/// </summary>
		public string Name{ get; set; }
		/// <summary>
		/// 生命值
		/// </summary>
		public int Health{ get; set; }
		/// <summary>
		/// 法力值
		/// </summary>
		public int Mana{ get; set; }
		/// <summary>
		/// 攻击力
		/// </summary>
		public int Attack{ get; set; }
		/// <summary>
		/// 护甲
		/// </summary>
		public int Armor{ get; set; }
		/// <summary>
		/// 暴击率
		/// </summary>
		public int CriticalRate{ get; set; }
		/// <summary>
		/// 攻速
		/// </summary>
		public int AttackSpeed{ get; set; }
		/// <summary>
		/// 测试数组
		/// </summary>
		public float[] TestArray{ get; set; }

        public MonsterData(JToken data)
        {
			MonsterType = data["MonsterType"].ToObject<int>();
			Name = data["Name"].ToObject<string>();
			Health = data["Health"].ToObject<int>();
			Mana = data["Mana"].ToObject<int>();
			Attack = data["Attack"].ToObject<int>();
			Armor = data["Armor"].ToObject<int>();
			CriticalRate = data["CriticalRate"].ToObject<int>();
			AttackSpeed = data["AttackSpeed"].ToObject<int>();
			TestArray = data["TestArray"].ToCustomArray<float>();
        }
    }
}
