using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Base;
using HybridCLR.Editor;
using HybridCLR.Editor.Commands;
using Unity.EditorCoroutines.Editor;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;

public class UpLoadWindow : EditorWindow
{
    [MenuItem("Tools/ClientTools/Upload &u")]
    private static void Open()
    {
        var win = GetWindow<UpLoadWindow>();
        win.maxSize = new Vector2(300, 400);
        win.minSize = win.maxSize;
    }
    private ClientConfig _clientConfig;
    private void Awake()
    {
        _clientConfig = Resources.Load<ClientConfig>("ClientConfig");
        ipAddress = _clientConfig.HotIpAddress;
        
        Debug.Log(GetBuildPlatform());
    }

    private string ipAddress = "http://127.0.0.1:8000";

    private void OnGUI()
    {
        GUILayout.Label("当前平台:"+GetBuildPlatform());
        GUILayout.Space(10);
        GUILayout.BeginHorizontal();

        GUILayout.Label("上传IP");
        ipAddress = GUILayout.TextField(ipAddress);

        GUILayout.EndHorizontal();
        GUILayout.Space(30);
        
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("编译目标平台Dll"))
        {
            CompileDllCommand.CompileDllActiveBuildTarget();
        }
        if (GUILayout.Button("编译目标平台Dll并上传"))
        {
            CompileDllCommand.CompileDllActiveBuildTarget();
            uploadDll();
        }
        if (GUILayout.Button("GenerateAll"))
        {
            PrebuildCommand.GenerateAll();
        }
        GUILayout.EndHorizontal();
        
        GUILayout.Space(30);
        GUILayout.BeginHorizontal();
        GUILayout.Label("上传");
        if (GUILayout.Button("Dlls"))
        {
            uploadDll();
        }
        if (GUILayout.Button("AssetBundles"))
        {
            uploadAB();
        }
        if (GUILayout.Button("AOT"))
        {
            UploadAOT();
        }

        if (GUILayout.Button("ALL"))
        {
            uploadDll();
            uploadAB();
            UploadAOT();
        }
        GUILayout.EndHorizontal();
        
    }

    private void uploadDll()
    {
        string src = Path.Combine("HybridCLRData", "HotUpdateDlls", GetBuildPlatform());
        UploadFile(src, "HotUpdateDlls", ".bytes",
            (fileName) => SettingsUtil.HotUpdateAssemblyFilesExcludePreserved.Contains(fileName));
    }

    private void uploadAB()
    {
        string src = Path.Combine("AssetBundles", GetBuildPlatform());
        UploadFile(src, "AssetBundles");
    }

    private void UploadAOT()
    {
        string src = Path.Combine("HybridCLRData", "AssembliesPostIl2CppStrip", GetBuildPlatform());
        UploadFile(src, "AOT", ".bytes");
    }

    private void UploadFile(string src, string serverPartner, string suffix="", Func<string,bool> find = null)
    {
        //需要上传的文件及对应的MD5
        var md5Data = GetMD5(src, out Dictionary<string, string> outPath, find);
        this.StartCoroutine(GetServerMD5(Path.Combine(serverPartner, "MD5"), serverMd5 =>
        {
            Dictionary<string, string> needUploadData = new Dictionary<string, string>(md5Data);
            foreach (var keyValuePair in md5Data)
            {
                var key = keyValuePair.Key;
                var value = keyValuePair.Value;

                if (serverMd5.ContainsKey(key+suffix) &&
                    serverMd5[key+suffix] == value) //与服务器一致就不上传了
                {
                    needUploadData.Remove(keyValuePair.Key);
                    outPath.Remove(keyValuePair.Key);
                }
            }
            if (outPath.Count == 0)
            {
                string m = ParseMD5(serverMd5);
                UpLoadSingle(Encoding.UTF8.GetBytes(m), serverPartner, "MD5", success =>
                {
                    Debug.Log("没有更改的文件，无需上传");
                });
                return;
            }

            int i = 0;
            foreach (var keyValuePair in outPath) //上传文件
            {
                UpLoadSingle(keyValuePair.Value, serverPartner, suffix,
                    success =>
                    {
                        if (success)
                        {
                            Debug.Log(keyValuePair.Value + "上传成功");
                            i++;
                            if (i == outPath.Count)
                            {
                                UpLoadSingle(Encoding.UTF8.GetBytes(ParseMD5(md5Data,suffix)), serverPartner, "MD5", success =>
                                {
                                    Debug.Log("全部上传完毕");
                                });
                            }
                        }
                        else
                        {
                            Debug.LogError(keyValuePair.Value + "上传失败");
                        }
                    });
            }
        }));
    }

    /// <summary>
    /// 获取服务器的MD5
    /// </summary>
    /// <param name="path">服务器相对路径</param>
    /// <param name="cb"></param>
    /// <returns></returns>
    private IEnumerator GetServerMD5(string path, Action<Dictionary<string, string>> cb)
    {
        UnityWebRequest request = new UnityWebRequest(ipAddress, UnityWebRequest.kHttpVerbPUT);
        request.downloadHandler = new DownloadHandlerBuffer();
        byte[] bytes1 = BitConverter.GetBytes(-1);
        byte[] bytes2 = Encoding.UTF8.GetBytes(Path.Combine(GetUploadSavePath(), path));
        byte[] byteAll = new byte[bytes1.Length + bytes2.Length];
        Array.Copy(bytes1, 0, byteAll, 0, bytes1.Length);
        Array.Copy(bytes2, 0, byteAll, bytes1.Length, bytes2.Length);
        request.uploadHandler = new UploadHandlerRaw(byteAll);
        yield return request.SendWebRequest();
        Dictionary<string, string> md5Data = new Dictionary<string, string>();
        if (request.result == UnityWebRequest.Result.Success)
        {
            string md5 = Encoding.UTF8.GetString(request.downloadHandler.data);
            if (md5 != "null")
            {
                var d = md5.Split("\n");
                foreach (var s in d)
                {
                    var d1 = s.Split(":");
                    md5Data.Add(d1[0], d1[1]);
                }
            }
            else
            {
                Debug.LogError("获取MD5失败");
            }
        }
        else
        {
            Debug.LogError("获取MD5失败" + request.error);
        }
        request.Dispose();
        cb?.Invoke(md5Data);
    }
    
    /// <summary>
    /// 获取MD5信息
    /// </summary>
    /// <param name="fileDir">相对路径</param>
    /// <param name="find">过滤文件</param>
    private Dictionary<string, string> GetMD5(string fileDir, out Dictionary<string, string> outPath,
        Func<string,bool> find = null)
    {
        outPath = new Dictionary<string, string>();
        string projectPath = Directory.GetParent(Application.dataPath).ToString();
        Dictionary<string, string> md5 = new Dictionary<string, string>();
        string uploadPath = Path.Combine(projectPath, fileDir);
        if (Directory.Exists(uploadPath))
        {
            string[] f = Directory.GetFiles(uploadPath);
            List<string> dllList = f.ToList();
            if (find != null)
            {
                dllList = dllList.FindAll(s => find(Path.GetFileName(s)));
            }

            foreach (var s in dllList)
            {
                md5.Add(Path.GetFileName(s), GetMD5HashFromFile(s));
                outPath.Add(Path.GetFileName(s), s);
            }
        }

        return md5;
    }

    /// <summary>
    /// 上传文件
    /// </summary>
    /// <param name="path">绝对路径</param>
    /// <param name="partnerFolder"></param>
    /// <param name="suffix"></param>
    /// <param name="cb"></param>
    private void UpLoadSingle(string path, string partnerFolder, string suffix = "", Action<bool> cb = null)
    {
        this.StartCoroutine(_UpLoad(path, partnerFolder, suffix, cb));
    }

    private void UpLoadSingle(byte[] data, string partnerFolder, string fileName, Action<bool> cb = null)
    {
        this.StartCoroutine(_UpLoad(data, partnerFolder, fileName, cb));
    }

    private IEnumerator _UpLoad(string path, string partnerFolder, string suffix = "", Action<bool> cb = null)
    {
        FileStream fs = File.Open(path, FileMode.Open);
        string fileName = Path.GetFileName(fs.Name); //获取文件名
        fileName = Path.Combine(GetUploadSavePath(), partnerFolder, fileName + suffix);

        MemoryStream ms = new MemoryStream();
        BinaryWriter bw = new BinaryWriter(ms);
        byte[] bName = System.Text.Encoding.UTF8.GetBytes(fileName);
        bw.Write(bName.Length);
        bw.Write(bName);

        BinaryReader br = new BinaryReader(fs);
        byte[] b = br.ReadBytes((int) fs.Length);
        bw.Write(b);

        byte[] result = new byte[ms.Length];
        Buffer.BlockCopy(ms.GetBuffer(), 0, result, 0, (int) ms.Length);

        fs.Close();
        bw.Close();
        ms.Close();
        br.Close();
        UnityWebRequest request = new UnityWebRequest(ipAddress, UnityWebRequest.kHttpVerbPUT);
        request.uploadHandler = new UploadHandlerRaw(result);
        request.SetRequestHeader("Content-Type", "application/octet-stream");
        yield return request.SendWebRequest();
        bool isSuccess = request.result == UnityWebRequest.Result.Success;
        if (request.result != UnityWebRequest.Result.Success)
        {
            Debug.LogError("上传失败" + request.error);
        }

        request.Dispose();
        cb?.Invoke(isSuccess);
    }

    private IEnumerator _UpLoad(byte[] bytes, string partnerFolder, string fileName, Action<bool> cb = null)
    {
        fileName = Path.Combine(GetUploadSavePath(), partnerFolder, fileName);
        MemoryStream ms = new MemoryStream();
        BinaryWriter bw = new BinaryWriter(ms);
        byte[] bName = System.Text.Encoding.UTF8.GetBytes(fileName);
        bw.Write(bName.Length);
        bw.Write(bName);
        bw.Write(bytes);

        byte[] result = new byte[ms.Length];
        Buffer.BlockCopy(ms.GetBuffer(), 0, result, 0, (int) ms.Length);

        bw.Close();
        ms.Close();
        UnityWebRequest request = new UnityWebRequest(ipAddress, UnityWebRequest.kHttpVerbPUT);
        request.uploadHandler = new UploadHandlerRaw(result);
        request.SetRequestHeader("Content-Type", "application/octet-stream");
        yield return request.SendWebRequest();
        bool isSuccess = request.result == UnityWebRequest.Result.Success;
        if (request.result != UnityWebRequest.Result.Success)
        {
            Debug.LogError("上传失败" + request.error);
        }

        request.Dispose();
        cb?.Invoke(isSuccess);
    }

    private string GetMD5HashFromFile(string filePath)
    {
        FileStream fs = new FileStream(filePath, FileMode.Open);
        MD5 md5 = new MD5CryptoServiceProvider();
        byte[] hash = md5.ComputeHash(fs);
        fs.Close();

        StringBuilder sb = new StringBuilder();
        foreach (var b in hash)
        {
            sb.Append(b.ToString("X2"));
        }

        return sb.ToString().ToUpper();
    }

    private string GetUploadSavePath()
    {
        return Path.Combine(Utils.GetSavePath(), GetBuildPlatform());
    }

    private string GetBuildPlatform()
    {
        // var buildTarget = EditorUserBuildSettings.activeBuildTarget;

        return _clientConfig.BuildPlatform.ToString(); // EditorUserBuildSettings.activeBuildTarget.ToString();
    }

    private string ParseMD5(Dictionary<string,string> data,string suffix="")
    {
        string md5 = "";
        int i = 0;
        foreach (var keyValuePair in data)
        {
            i++;
            md5 += keyValuePair.Key +suffix+ ":" + keyValuePair.Value + (i < data.Count ? "\n" : "");
        }

        return md5;
    }

    private Dictionary<string, string> ParseMD5(string data)
    {
        Dictionary<string, string> d = new Dictionary<string, string>();
        
        foreach (var s in data.Split("\n"))
        {
            var kv = s.Split(":");
            d.Add(kv[0],kv[1]);
        }
        return d;
    }
}