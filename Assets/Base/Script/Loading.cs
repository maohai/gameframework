using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using HybridCLR;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace Base
{
    public class Loading : MonoBehaviour
    {
        [SerializeField] private Text txtTips;

        [SerializeField] private Slider progressSlider;

        private float progress = 0;

        private string url = "http://127.0.0.1:8000";

        private float total;
        private float curProgress;

        private Dictionary<string, string> saveMD5Data = new Dictionary<string, string>();
        private Dictionary<string, string> _serverMD5Data = new Dictionary<string, string>();
        private Dictionary<string, string> _localMD5Data = new Dictionary<string, string>();

        private int maxCount = 0;

        private bool isStartUpdate = false;
        private void Start()
        {
            txtTips.text = "";
            progressSlider.value = 0;
            url = Resources.Load<ClientConfig>("ClientConfig").HotIpAddress;
            Debug.Log("连接热更服务器"+url);
            Debug.Log("检测更新路径"+Utils.GetSavePlatformPath());
            CheckUpdate();
        }

        private Dictionary<string, string> GetMD5Data(string data)
        {
            Dictionary<string, string> md5Data = new Dictionary<string, string>();
            foreach (var s in data.Split("\n"))
            {
                var d = s.Split(":");
                md5Data.Add(d[0],d[1]);
            }

            return md5Data;
        }

        
        
        private void CheckUpdate()
        {
            DownloadFile("MD5", data =>
            {
                if (data!=null)
                {
                    //服务器MD5
                    Dictionary<string, string> serverMD5Data = GetMD5Data(Encoding.UTF8.GetString(data));
                    //本地MD5
                    string localMD5Path = Path.Combine(Application.persistentDataPath, "Update/MD5");
                    if (File.Exists(localMD5Path))
                    {
                        string md5 = File.ReadAllText(localMD5Path,Encoding.UTF8);
                        _localMD5Data = GetMD5Data(md5);
                    }

                    _serverMD5Data = new Dictionary<string, string>(serverMD5Data);
                    maxCount = _serverMD5Data.Count;
                    saveMD5Data.Clear();
                    foreach (var keyValuePair in serverMD5Data)
                    {
                        string key = keyValuePair.Key;
                        string value = keyValuePair.Value;
                        if (_localMD5Data.ContainsKey(key) && _localMD5Data[key] == value)//相同 则不用更新
                        {
                            _localMD5Data.Remove(key);
                            _serverMD5Data.Remove(key);
                            saveMD5Data.Add(key,value);
                        }
                        else
                        {
                            Debug.Log(key);
                            DownloadFile(key, "Update/" + key, success =>
                            {
                                if (success)
                                {
                                    _localMD5Data.Remove(key);
                                    _serverMD5Data.Remove(key); 
                                    saveMD5Data.Add(key,value);
                                }
                            });
                        }
                    }
                    // string serverMD5=System.Text.Encoding.UTF8.GetString(request.downloadHandler.data);
  
                }
                else
                {
                    Debug.LogError("下载MD5失败");
                }

                isStartUpdate = true;
            });
 
        }

        private void OnUpdateCompile()
        {
            Debug.Log("热更新完成");
            foreach (var keyValuePair in _localMD5Data)//多余文件删掉
            {
                string p = Path.Combine(Application.persistentDataPath, "Update", keyValuePair.Key);
                if (File.Exists(p))
                {
                    File.Delete(p);
                }
            }
            //保存MD5
            string md5 = "";
            int i = 0;
            foreach (var keyValuePair in saveMD5Data)
            {
                i++;
                md5 += keyValuePair.Key + ":" + keyValuePair.Value + (i<saveMD5Data.Count?"\n":"");
            }

            if (!string.IsNullOrEmpty(md5))
            {
                FileStream fs = new FileStream(Path.Combine(Application.persistentDataPath, "Update", "MD5"), FileMode.Create);
                //获得字节数组
                byte[] data = Encoding.Default.GetBytes(md5); 
                //开始写入
                fs.Write(data, 0, data.Length);
                //清空缓冲区、关闭流
                fs.Flush();
                fs.Close();
            }

            LoadAOT();
            LoadStartDll();
        }

        private void DownloadFile(string path, string savePath,Action<bool> cb)
        {
            StartCoroutine(_DownloadFile(path, data =>
            {
                if (data != null)
                {
                    // Debug.Log("保存"+savePath);
                    SaveFile(savePath,data);
                    cb?.Invoke(true);
                }
                else
                {
                    Debug.LogError("保存"+savePath+"失败");
                    cb?.Invoke(false);
                }
            }));
        }

        private void DownloadFile(string path,Action<byte[]> cb)
        {
            StartCoroutine(_DownloadFile(path, data =>
            {
                cb?.Invoke(data);
            }));
        }

        private IEnumerator _DownloadFile(string path,Action<byte[]> cb)
        {
            UnityWebRequest request = new UnityWebRequest(url, UnityWebRequest.kHttpVerbPOST);
            Debug.Log("下载路径"+Path.Combine(GetSavePath(), path));
            request.uploadHandler =
                new UploadHandlerRaw(Encoding.UTF8.GetBytes(Path.Combine(GetSavePath(), path)));
            request.downloadHandler = new DownloadHandlerBuffer();
            request.SetRequestHeader("Content-Type", "application/octet-stream");
            yield return request.SendWebRequest();

            if (request.result == UnityWebRequest.Result.Success)
            {
                cb?.Invoke(request.downloadHandler.data);
            }
            else
            {
                Debug.Log(request.error);
                cb?.Invoke(null);
            }
            request.Dispose();
        }

        private void SaveFile(string path, byte[] data)
        {
            path = Path.Combine(Application.persistentDataPath, path);
            // Debug.Log(path);
            string parent = Directory.GetParent(path).ToString();
            if (!Directory.Exists(parent))
            {
                Directory.CreateDirectory(parent);
            }

            File.WriteAllBytes(path, data);
            Debug.Log(path + "下载成功");
        }

        private string GetSavePath()
        {
            return Utils.GetSavePlatformPath();
        }

        private void LoadStartDll()
        {
#if UNITY_EDITOR
            Assembly clientModule = AppDomain.CurrentDomain.GetAssemblies().First(a => a.GetName().Name == "ClientModule");
#else
            string p1=Path.Combine(Application.persistentDataPath,"Update", "HotUpdateDlls", "ClientModule.dll.bytes");
            var clientModule = Assembly.Load(File.ReadAllBytes(p1));
#endif
            clientModule.GetType("GameClient.Client").GetMethod("Run").Invoke(null, null);
        }

        private void LoadAOT()
        {
#if !UNITY_EDITOR
            var files = Directory.GetFiles(Path.Combine(Application.persistentDataPath, "Update","AOT"));
            foreach (var file in files)
            {
                if (Path.GetFileName(file) != "MD5")
                {
                    // Debug.Log("加载AOT "+file);
                    byte[] dllBytes = File.ReadAllBytes(file);
                    RuntimeApi.LoadMetadataForAOTAssembly(dllBytes, HomologousImageMode.SuperSet);
                }
            }
#endif
        }
        
        private void Update()
        {
            if (!isStartUpdate)
            {
                return;
            }

            txtTips.text = (maxCount-_serverMD5Data.Count) + "/" + maxCount;
            float p = (maxCount-_serverMD5Data.Count)*1f/maxCount;
            progress = Mathf.Lerp(progress, p, 2*Time.deltaTime);
            progressSlider.value = progress;
            if (_serverMD5Data.Count == 0)
            {
                isStartUpdate = false;
                OnUpdateCompile();
                Destroy(transform.parent.gameObject);
            }
        }
    }
}