using System.IO;
using UnityEngine;

namespace Base
{
    public class Utils
    {
        public static string GetSavePlatformPath()
        {
            return Path.Combine(Application.companyName, Application.productName, GetPlatformPath());
        }

        public static string GetSavePath()
        {
            return Path.Combine(Application.companyName, Application.productName);
        }

        public static string GetFullSavePath()
        {
            return Path.Combine(Application.persistentDataPath, GetSavePath());
        }

        public static string GetPlatformPath()
        {
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                    return "Android";
                case RuntimePlatform.WindowsEditor:
                case RuntimePlatform.WindowsPlayer:
                default:
                    return "StandaloneWindows64";
            }
        }
    }

}