from http.server import BaseHTTPRequestHandler, HTTPServer,ThreadingHTTPServer
import os
def saveFile(path:str,data:bytes):
    fullFilePath=os.path.join(os.path.split(os.path.realpath(__file__))[0],path)
    dirName=os.path.dirname(fullFilePath)
    if not os.path.exists(dirName):
        os.makedirs(dirName)
    with open(fullFilePath,'wb') as f:
        f.write(data)

    # print("上传到"+fullFilePath)
    

def getMD5(path):
    strMd5=""
    if os.path.exists("./"+path):
        with open(path,'r') as f:
            lines=f.readlines()
            for line in lines:
                strMd5 +=line
    return strMd5

def getAllMd5(path):
    strMD5=""
    i=0
    for root, dirs, files in os.walk("./"+path, topdown=False):
        # print(root)
        for dir in dirs:
            # print(dir)
            dirPath =os.path.join(root,dir,"MD5")
            i+=1
            if os.path.exists(dirPath):
                with open(dirPath,'r') as f:
                    lines=f.readlines()
                    for line in lines:
                        strMD5 +=dir+"/"+line
                if i < len(dirs):
                    strMD5+="\n"


    return strMD5


# 创建自定义的请求处理类
class FileUploadHandler(BaseHTTPRequestHandler):

    def getSingleMD5(self,fileName):
        #获取单独MD5
        return getMD5(fileName)

    #用于下载文件
    def do_POST(self):
        length = int(self.headers['content-length'])  # 获取除头部后的请求参数的长度
        datas=self.rfile.read(length)
        fileName=datas.decode(encoding="utf-8")
        # print(fileName)
        if os.path.basename(fileName)=="MD5":#整个md5
            data = bytes(getAllMd5(os.path.dirname(fileName)),'utf-8')
            self.send_response(200)
            self.end_headers()
            self.wfile.write(data)
            return

        if os.path.exists("./"+fileName):
            with open("./"+fileName,'rb') as f:
                data = f.read()
                self.send_response(200)
                self.end_headers()
                self.wfile.write(data)
                print(fileName+"下载成功")
                return
        self.send_response(404)
        self.end_headers()
        self.wfile.write(b'File download fail.')
        print(fileName+"下载失败")


    def put_MD5(self,path):
        md5=getMD5(path)
        self.send_response(200)
        self.end_headers()
        if md5=="":
            md5="null"
        self.wfile.write(bytes(md5,encoding='utf-8'))

    def clearOtherByMD5(self,fileName,md5:str):
        # print("清理其他"+fileName)
        files=list()
        for a in md5.split("\n"):
            files.append(a.split(":")[0])

        fileDir="./"+os.path.dirname(fileName)
        curfiles:list = os.listdir(fileDir)
        if "MD5" in curfiles:
            curfiles.remove("MD5")
        for  f in curfiles:
            if f not in files:
                os.remove(os.path.join(fileDir,f))


    #用于编辑器下上传文件
    def do_PUT(self):
        # start_time = time.time()
        length = int(self.headers['content-length'])  # 获取除头部后的请求参数的长度
        datas = self.rfile.read(4) # 读取文件名长度
        lenName = int.from_bytes(datas,byteorder="little",signed=True)
        if lenName==-1:#需要发送MD5给客户端用于校验哪些需要上传
            self.put_MD5(self.rfile.read(length-4).decode(encoding="utf-8"))
            return
        datas = self.rfile.read(lenName)# 读取文件夹+文件名
        fileName=datas.decode(encoding="utf-8")
        datas = self.rfile.read(length-4-lenName) #读取文件
      

        # 下载了新的MD5 根据客户端的MD5会清理无用的文件
        if os.path.basename(fileName)=="MD5":
            self.clearOtherByMD5(fileName,datas.decode(encoding="utf-8"))

        # 在这里可以对接收到的文件数据进行处理，例如保存到磁盘
        saveFile(fileName,datas)
   
        self.send_response(200)
        self.end_headers()
        self.wfile.write(b'File uploaded successfully.')


 
        # end_time = time.time()
        # time_elapsed_ms = int((end_time - start_time) * 1000)
        # print(f"Update in {time_elapsed_ms} ms")
 
 
# 启动服务器
def run_server():
    server_address = ('127.0.0.1', 8000)  # 可以根据需要修改端口号
    httpd = ThreadingHTTPServer(server_address, FileUploadHandler)
    print('Server running on port 8000...')
    httpd.serve_forever()
 
 
# 运行服务器
run_server()