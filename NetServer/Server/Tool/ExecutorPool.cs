using System.Threading;

namespace Server.Tool
{
    public delegate void ExecutorDelegate();
    public class ExecutorPool
    {
        /// <summary>
        /// 线程同步锁
        /// </summary>
        private Mutex tex = new Mutex();

        private static ExecutorPool ins;

        public static ExecutorPool Ins
        {
            get
            {
                if (ins == null)
                {
                    ins = new ExecutorPool();
                }

                return ins;
            }
        }

        /// <summary>
        /// 单线程处理逻辑
        /// </summary>
        /// <param name="d"></param>
        public void Execute(ExecutorDelegate d)
        {
            lock (this)
            {
                tex.WaitOne();
                d();
                tex.ReleaseMutex();;
            }
        }
    }
}