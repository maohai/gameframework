using DataBase.Model;

namespace Server.Model
{
    public class AccountModel:IDataModel
    {
        [DataInfo("Account",DataType.VARCHAR,30,false,null,true)]
        public string account;
        
        [DataInfo("Password",DataType.VARCHAR,30)]
        public string password;
        
        [DataInfo("NickName",DataType.VARCHAR,30,true)]
        public string nickname;
    }
}