using BaseNetServer;
using GameClient.NetWork;
using Server.Biz;
using Server.Cache;

namespace Server
{
    public class GameServer:BaseServer
    {
        public override PropType GetPropType()
        {
            return PropType.GAME;
        }

        public override void MessageReceive(UserToken token, SocketModel message)
        {
            switch (message.command)
            {
                case  GameProp.TRANSFORM_CREQ:
                    Transform(token,message.GetMessage<TransformDto>());
                    break;
            }
        }


        private void Transform(UserToken token,TransformDto dto)
        {
            var tokens = CacheFactory.AccountCache.GetOnlineUser();
            for (int i = 0; i < tokens.Count; i++)
            {
                if (tokens[i] != token)
                {
                    Write(tokens[i],GameProp.TRANSFORM_SERS,new MsgDto(true,"位置同步成功"));
                }
            }
        }
    }
}