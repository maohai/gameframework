using BaseNetServer;
using GameClient.NetWork;
using Server.Biz;
using Server.Cache;
using Server.Tool;

namespace Server
{
    public class LoginServer : BaseServer
    {

        private int id = 10000;
        
        public override PropType GetPropType()
        {
            return PropType.LOGIN;
        }
        
        

        public override void ClientConnect(UserToken token)
        {
            base.ClientConnect(token);
            CacheFactory.AccountCache.Online(token,(++id)+"");
            Write(token,LoginProp.LOGIN_SRES,new MsgDto(true,id.ToString()));
        }

        public override void MessageReceive(UserToken token, SocketModel message)
        {
            switch (message.command)
            {
                case  LoginProp.LOGIN_CREQ:
                    Login(token,message.GetMessage<LoginInfoDto>());
                    break;
                case LoginProp.REGISTER_CREQ:
                    Register(token,message.GetMessage<RegisterInfoDto>());
                    break;
            }
            
        }

        public override void ClientClose(UserToken token, string error)
        {
            // BizFactory.AccountBiz.Close(token);
            CacheFactory.AccountCache.Offline(token);
        }

        //登录
        private void Login(UserToken token,LoginInfoDto info)
        {
            ExecutorPool.Ins.Execute(() =>
            {
                MsgDto r= BizFactory.AccountBiz.Login(token,info);
                Write(token,LoginProp.LOGIN_SRES,r);
            });
        }
        
        //注册
        private void Register(UserToken token,RegisterInfoDto info)
        {
            ExecutorPool.Ins.Execute(() =>
            {
                MsgDto r= BizFactory.AccountBiz.Create(token,info);
                if (r.success)
                {
                    Debug.Log(info.account+"上线了");
                    Write(token,LoginProp.REGISTER_SRES,r);
                }
                else
                {
                    ReplyMsg(token,new MsgDto(false,"注册失败了"));
                }
            });
        }
        
    }
}