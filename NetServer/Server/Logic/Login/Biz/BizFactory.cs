
namespace Server.Biz
{
    public class BizFactory
    {
        public readonly static AccountBiz AccountBiz;

        static BizFactory()
        {
            AccountBiz = new AccountBiz();
        }
    }
}