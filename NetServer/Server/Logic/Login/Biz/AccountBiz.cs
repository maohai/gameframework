using BaseNetServer;
using GameClient.NetWork;
using Server.Cache;

namespace Server.Biz
{
    public class AccountBiz
    {
        /// <summary>
        /// 注册
        /// </summary>
        /// <param name="token"></param>
        /// <param name="info"></param>
        /// <returns></returns>
        public MsgDto Create(UserToken token, RegisterInfoDto info)
        {
            bool a = CacheFactory.AccountCache.Create(info);
            if (a)
            {
                return new MsgDto(true,"注册成功");
            }
            return new MsgDto(false,"注册失败");
        }

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="token"></param>
        /// <param name="info"></param>
        /// <returns></returns>
        public MsgDto Login(UserToken token,LoginInfoDto info)
        {
            if (string.IsNullOrEmpty(info.account) || string.IsNullOrEmpty(info.password))
            {
                return new MsgDto(false,"账号密码不合法");//账号密码不合法
            }
            bool isHave = CacheFactory.AccountCache.HasAccount(info.account);
            if (!isHave)
            {
                return new MsgDto(false,"该账号不存在");
            }

            bool ok = CacheFactory.AccountCache.Verify(info.account,info.password);
            if (ok)
            {
                if (CacheFactory.AccountCache.IsOnline(info.account))
                {
                    return new MsgDto(false,"重复登录");
                }
                CacheFactory.AccountCache.Online(token,info.account);
                return new MsgDto(true,"登录成功");
            }
            return new MsgDto(false,"密码错误");
        }

        /// <summary>
        /// 用户下线
        /// </summary>
        /// <param name="token"></param>
        public void Close(UserToken token)
        {
            CacheFactory.AccountCache.Offline(token);
        }
    }
}