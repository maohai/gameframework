using BaseNetServer;
using GameClient.NetWork;
using Server.Cache;

namespace Server
{
    public abstract class BaseServer:IHandler
    {
        public abstract PropType GetPropType();

        public virtual int GetArea()
        {
            return -1;
        }

        public virtual void ClientConnect(UserToken token)
        {
        }

        public virtual void ClientClose(UserToken token, string error)
        {
        }

        public virtual void MessageReceive(UserToken token, SocketModel message)
        {
        }

        #region 通过连接对象发送
        public void Write(UserToken token,int command)
        {
            Write(token,command,null);
        }
        public void Write(UserToken token,int command,object message)
        {
            Write(token,GetPropType(),command,message);
        }
        public void Write(UserToken token,PropType type,int command,object message)
        {
            Write(token,type,GetArea(),command,message);
        }
        public void Write(UserToken token,PropType type,int area,int command,object message)
        {
            byte[] value = MessageEncoding.Encode(new SocketModel(token.MsgID,type, area, command, message));
            value = LengthEncoding.Encode(value);
            token.Write(value);
        }

        /// <summary>
        /// 回复
        /// </summary>
        /// <param name="token"></param>
        /// <param name="message"></param>
        public void ReplyMsg(UserToken token, MsgDto message)
        {
            byte[] value = MessageEncoding.Encode(new SocketModel(token.MsgID,PropType.Message, GetArea(), 0 , message));
            value = LengthEncoding.Encode(value);
            token.Write(value);
        }
        
        
        /// <summary>
        /// 推送
        /// </summary>
        /// <param name="token"></param>
        /// <param name="command"></param>
        /// <param name="message"></param>
        public void Push(UserToken token,int command,object message)
        {
            Push(token,PropType.Message,GetArea(),command,message);
        }
        
        public void Push(UserToken token,PropType type,int area,int command,object message)
        {
            byte[] value = MessageEncoding.Encode(new SocketModel(-1,type, area, command, message));
            value = LengthEncoding.Encode(value);
            token.Write(value);
        }
        #endregion
        
        
        #region 通过对象id发送
        public void Write(string id,int command)
        {
            UserToken token = CacheFactory.AccountCache.GetUserTokenById(id);
            if (token!=null)
            {
                Write(token,command);
            }
   
        }
        public void Write(string id,int command,object message)
        {
            UserToken token = CacheFactory.AccountCache.GetUserTokenById(id);
            if (token != null)
            {
                Write(token,command,message);
            }

        }
        public void Write(string id,PropType type,int command,object message)
        {
            UserToken token = CacheFactory.AccountCache.GetUserTokenById(id);
            if (token != null)
            {
                Write(token,type,command,message);
            }
        
        }
        public void Write(string id,PropType type,int area,int command,object message)
        {
            UserToken token = CacheFactory.AccountCache.GetUserTokenById(id);
            if (token != null)
            {
                Write(token,type,area,command,message);
            }
        }
        
        #endregion

    }
}