using BaseNetServer;
using Net;
using Net.Dto;
using Net.Protocol;
using Server.Biz;
using Server.Tool;

namespace Server
{
    public class RoomServer:BaseServer
    {
        public override PropType GetPropType()
        {
            return PropType.ROOM;
        }

        public override void MessageReceive(UserToken token, SocketModel message)
        {
            switch (message.command)
            {
                case RoomProp.JOIN_CREQ:
                    Join(token,message.GetMessage<JoinOrCreateRoomDto>());
                    break;
                case RoomProp.CREATE_CREQ:
                    Create(token,message.GetMessage<JoinOrCreateRoomDto>());
                    break;
            }
        }
        /// <summary>
        /// 加入房间
        /// </summary>
        /// <param name="token"></param>
        /// <param name="dto"></param>
        private void Join(UserToken token,JoinOrCreateRoomDto dto)
        {
            ExecutorPool.Ins.Execute(() =>
            {
                RoomInfoDto r= BizFactory.RoomBiz.Join(token,dto);
                Write(token,RoomProp.JOIN_SRES,r);
            });
        }
        /// <summary>
        /// 创建房间
        /// </summary>
        /// <param name="token"></param>
        /// <param name="dto"></param>
        private void Create(UserToken token, JoinOrCreateRoomDto dto)
        {
            ExecutorPool.Ins.Execute(() =>
            {
                RoomInfoDto r= BizFactory.RoomBiz.Create(token,dto);
                Write(token,RoomProp.CREATE_SERS,r);
            });
        }

        private void Move(UserToken token)
        {
            
        }

    }
}