﻿using BaseNetServer;
using GameClient.NetWork;
using Server.Biz;
using Server.Cache;

namespace Server
{
    public class ChatServer:BaseServer
    {
        public override PropType GetPropType()
        {
            return PropType.CHAT;
        }

        public override void MessageReceive(UserToken token, SocketModel message)
        {
            switch (message.command)
            {
                case ChatProp.MESSAGE_CREQ:
                    OnMessage(token,message.GetMessage<MessageDto>());
                    break;
            }
        }

        private void OnMessage(UserToken token,MessageDto dto)
        {
            var online = CacheFactory.AccountCache.GetOnlineUser();

            foreach (var userToken in online)
            {
                if (userToken != token)
                {
                    Write(userToken,ChatProp.MESSAGE_SERS,dto);
                }
            }
        }
    }
}