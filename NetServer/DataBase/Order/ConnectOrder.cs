using System;
using System.Collections.Generic;

namespace DataBase.Order
{
    public class ConnectOrder : BaseOrder
    {
        public DataBaseType dataBaseType
        {
            get => DataBaseConfig.dataBaseType;
            set => DataBaseConfig.dataBaseType = value;
        }

        public string datasource
        {
            get => DataBaseConfig.datasource;
            set => DataBaseConfig.datasource = value;
        }

        public int port
        {
            get => DataBaseConfig.port;
            set => DataBaseConfig.port = value;
        }

        public string user
        {
            get => DataBaseConfig.user;
            set => DataBaseConfig.user = value;
        }

        public string pwd
        {
            get => DataBaseConfig.pwd;
            set => DataBaseConfig.pwd = value;
        }

        public string dataBaseName
        {
            get => DataBaseConfig.dataBaseName;
            set => DataBaseConfig.dataBaseName = value;
        }

        public Func<Dictionary<string, Type>> tablsData
        {
            get => DataBaseConfig.GetTableData;
            set => DataBaseConfig.GetTableData = value;
        }

        internal ConnectOrder()
        {
        }

        public override bool Execute()
        {
            if (DataBaseConfig.IsConnected)
            {
                return false;
            }

            _dataBase.Connect(DataBaseConfig.dataBaseName);
            _dataBase.Init();
            DataBaseConfig.IsConnected = true;
            return true;
        }
    }
}