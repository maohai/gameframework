using System;
using System.Collections.Generic;
using DataBase.Model;

namespace DataBase.Order
{
    
    public class OrderFactory
    {
        private static readonly Dictionary<Type, BaseOrder> _orders = new Dictionary<Type, BaseOrder>();

        public static T GetOrder<T>() where T: BaseOrder
        {
            Type type = typeof(T);

            if (_orders.ContainsKey(type))
            {
                return (T)_orders[type];
            }

            BaseOrder order = null;
            if (type == typeof(ConnectOrder))
            {
                order = new ConnectOrder();
            }
            else if (type == typeof(DeleteOrder))
            {
                order = new DeleteOrder();
            }
            else if (type == typeof(InsertOrder))
            {
                order = new InsertOrder();
            }
            else if (type == typeof(UpdateOrder))
            {
                order = new UpdateOrder();
            }
            
            if (order != null)
            {
                _orders.Add(type,order);
            }
            return (T)order;
        }

        public static SelectOrder<T> GetSelectOrder<T>() where T : IDataModel
        {
            var t = typeof(SelectOrder<T>);
            if (_orders.ContainsKey(t))
            {
                return _orders[t] as SelectOrder<T>;
            }

            var order = new SelectOrder<T>();
            _orders.Add(t,order);
            return order;
        }
    }
}