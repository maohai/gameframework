using DataBase.Model;

namespace DataBase.Order
{
    public class InsertOrder:BaseOrder
    {
        private IDataModel _model;
        internal InsertOrder()
        {
        }

        public InsertOrder Bind(string tableName, IDataModel model)
        {
            _tableName = tableName;
            _model = model;

            return this;
        }
        
        public override bool Execute()
        {
            return _dataBase.Insert(_tableName, _model);
        }
    }
}