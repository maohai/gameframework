using System;

namespace DataBase.Model
{
    public enum DataType
    {
        /// <summary>
        /// (-128,127) 小整数型
        /// </summary>
        TINYINT,
        /// <summary>
        /// 大整数值
        /// </summary>
        INT,
        /// <summary>
        /// 极大整数值
        /// </summary>
        BIGINT,
        /// <summary>
        /// 单精度浮点值
        /// </summary>
        FLOAT,
        /// <summary>
        /// 双精度浮点数值
        /// </summary>
        DOUBLE,
        
        /// <summary>
        /// 日期值
        /// </summary>
        DATE,
        /// <summary>
        /// 混合日期和时间值
        /// </summary>
        DATETIME,
        
        /// <summary>
        /// 定长字符串
        /// </summary>
        CHAR,
        /// <summary>
        /// 变长字符串
        /// </summary>
        VARCHAR,
        
    }
    
    public class DataInfo:Attribute
    {
        private string _dataName;
        private int _size;
        private DataType _dataType;
        private bool _isKey;
        private bool _isNull;
        private object _default=null;

        public string DataName => _dataName;

        // public int Size => _size;

        public DataType DataType => _dataType;

        public bool IsNull => _isNull;

        public bool IsKey => _isKey;

        public object Default => _default;

        public DataInfo(string name, DataType dataType,int size=0,bool isNull=false,object @default=null,bool isKey = false)
        {
            _dataName = name;
            _dataType = dataType;
            _isNull = isNull;
            _size = size;
            _isKey = isKey;
            _default = @default;
        }
        
        public string GetDataType()
        {
            if (_dataType == DataType.VARCHAR)
            {
                return _dataType+ $"({_size})";
            }

            return _dataType.ToString();
        }

        public string GetIsNULL()
        {
            return IsNull ? "NULL" : "NOT NULL";
        }
    }
}