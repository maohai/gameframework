using System.Collections.Generic;

namespace GameClient.NetWork
{
    public delegate byte[] LengthEncode(byte[] buff);

    public delegate byte[] LengthDecode(ref List<byte> cache);

    public delegate byte[] Encode(SocketModel value);

    public delegate SocketModel Decode(byte[] value);
}