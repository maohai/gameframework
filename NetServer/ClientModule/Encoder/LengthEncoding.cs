using System;
using System.Collections.Generic;
using System.IO;

namespace GameClient.NetWork
{
    public class LengthEncoding
    {
        /// <summary>
        /// 编码
        /// </summary>
        /// <param name="buff"></param>
        /// <returns></returns>
        public static byte[] Encode(byte[] buff)
        {
            MemoryStream ms = new MemoryStream();//创建内存流
            BinaryWriter bw = new BinaryWriter(ms);//写入二进制对象流
            bw.Write(buff.Length);
            bw.Write(buff);
            byte[] result = new byte[ms.Length];
            Buffer.BlockCopy(ms.GetBuffer(),0,result,0,(int)ms.Length);
            ms.Close();
            bw.Close();
            return result;
        }

        /// <summary>
        /// 解码
        /// </summary>
        /// <param name="chche"></param>
        /// <returns></returns>
        public static byte[] Decode(ref List<byte> cache)
        {
            if (cache.Count < 4) return null;
            MemoryStream ms = new MemoryStream(cache.ToArray());
            BinaryReader br = new BinaryReader(ms);
            int length = br.ReadInt32();
            //消息体长度大于缓存的长度，说明没读取完 等待下次消息体到达后再次处理
            if (length > ms.Length - ms.Position)
            {
                return null;
            }
            byte[] result = br.ReadBytes(length);
            cache.Clear();
            cache.AddRange(br.ReadBytes((int)(ms.Length-ms.Position)));
            br.Close();
            ms.Close();
            return result;

        }
    }
}