﻿using System;

namespace GameClient.NetWork
{
    /// <summary>
    /// 消息协议
    /// </summary>
    [Serializable]
    public class MessageDto
    {
        public string Id;
        public string Msg;
    }
}