using System.Diagnostics;
using GameClient.NetWork;

namespace GameClient.NetWork
{
    public class SocketModel
    {
        public long id { get; set; }
        /// <summary>
        /// 一级协议 用于区分模块
        /// </summary>
        public PropType type { get; set; }
        
        /// <summary>
        /// 二级协议 用于区分子模块
        /// </summary>
        public int area { get; set; }
        
        /// <summary>
        /// 三级协议 用于区分当前逻辑功能
        /// </summary>
        public int command { get; set; }
        
        /// <summary>
        /// 消息体 当前需要处理的对象
        /// </summary>
        public object message { get; set; }

        public SocketModel()
        {
            
        }

        public SocketModel(long i, PropType t, int a, int c, object o)
        {
            id = i;
            type = t;
            area = a;
            command = c;
            message = o;
        }

        public T GetMessage<T>()
        {
            return (T) message;
        }

        public override string ToString()
        {
            string msg = "";
            var field = message.GetType().GetFields();
            for (int i = 0; i < field.Length; i++)
            {
                msg += field[i].Name + ":" + field[i].GetValue(message);
                if (i != field.Length - 1)
                {
                    msg += ";";
                }
            }
            return string.Format("id:{0},type:{1},area:{2},command:{3},message:{4}",id, type, area, command, msg);
        }
    }
}